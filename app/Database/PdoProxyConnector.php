<?php
namespace App\Database;

use Illuminate\Database\Connectors\MySqlConnector;
use Exception;
use pdoProxy;

class PdoProxyConnector extends MySqlConnector
{

    protected function setModesProxy(pdoProxy $connection, array $config)
    {
        if (isset($config['modes'])) {
            $this->setCustomModes($connection, $config);
        } elseif (isset($config['strict'])) {
            if ($config['strict']) {
                $connection->prepare($this->strictModeProxy())->execute();
            } else {
                $connection->prepare("set session sql_mode='NO_ENGINE_SUBSTITUTION'")->execute();
            }
        }
    }
    
    public function connect(array $config)
    {
        $dsn = $this->getDsn($config);
        
        $options = $this->getOptions($config);
        
        // We need to grab the PDO options that should be used while making the brand
        // new connection instance. The PDO options control various aspects of the
        // connection's behavior, and some might be specified by the developers.
        $connection = $this->createConnection($dsn, $config, $options);
        
        if (! empty($config['database'])) {
            $connection->exec("use `{$config['database']}`;");
        }
        
        $this->configureEncoding($connection, $config);
        
        // Next, we will check to see if a timezone has been specified in this config
        // and if it has we will issue a statement to modify the timezone with the
        // database. Setting this DB timezone is an optional configuration item.
        $this->configureTimezone($connection, $config);
        
        $this->setModesProxy($connection, $config);
        
        return $connection;
    }
    public function createConnection($dsn, array $config, array $options)
    {
        [$username, $password] = [
            $config['username'] ?? null, $config['password'] ?? null,
        ];
        try {
            return new pdoProxy($dsn, $username, $password, $options);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
    protected function strictModeProxy()
    {
        return "set session sql_mode='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION'";
    }
    
}