<?php
namespace App\Database;
use PDO;
use pdo_connect_pool_PDOStatement;
use Illuminate\Database\MySqlConnection;
use Illuminate\Database\Events\StatementPrepared;
class PdoProxyConnection extends MySqlConnection
{
    
    
    protected $fetchMode = PDO::FETCH_ASSOC;

    protected function swooleProxyPrepared(pdo_connect_pool_PDOStatement $statement)
    {
        $statement->setFetchMode($this->fetchMode);
        $this->event(new StatementPrepared(
            $this, $statement
        ));
        return $statement;
    }
    /**
     * Run a select statement against the database.
     *
     * @param  string  $query
     * @param  array  $bindings
     * @param  bool  $useReadPdo
     * @return array
     */
    public function select($query, $bindings = [], $useReadPdo = true)
    {
        return $this->run($query, $bindings, function ($query, $bindings) use ($useReadPdo) {
            if ($this->pretending()) {
                return [];
            }
            // For select statements, we'll simply execute the query and return an array
            // of the database result set. Each element in the array will be a single
            // row from the database table, and will either be an array or objects.
            $statement = $this->swooleProxyPrepared($this->getPdoForSelect($useReadPdo)
                              ->prepare($query));
            $this->bindValues($statement, $this->prepareBindings($bindings));
            $statement->execute();
            return $statement->fetchAll();
        });
    }
    /**
     * Run a select statement against the database and returns a generator.
     *
     * @param  string  $query
     * @param  array  $bindings
     * @param  bool  $useReadPdo
     * @return \Generator
     */
    public function cursor($query, $bindings = [], $useReadPdo = true)
    {
        $statement = $this->run($query, $bindings, function ($query, $bindings) use ($useReadPdo) {
            if ($this->pretending()) {
                return [];
            }
            // First we will create a statement for the query. Then, we will set the fetch
            // mode and prepare the bindings for the query. Once that's done we will be
            // ready to execute the query against the database and return the cursor.
            $statement = $this->swooleProxyPrepared($this->getPdoForSelect($useReadPdo)
                              ->prepare($query));
            $this->bindValues(
                $statement, $this->prepareBindings($bindings)
            );
            // Next, we'll execute the query against the database and return the statement
            // so we can return the cursor. The cursor will use a PHP generator to give
            // back one row at a time without using a bunch of memory to render them.
            $statement->execute();
            return $statement;
        });
        while ($record = $statement->fetch()) {
            yield $record;
        }
    }
}